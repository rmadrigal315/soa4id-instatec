const express = require("express");
const app = express();
const port = 3000;

app.get("/hello-world", (req, res) => res.send("hello world"));
app.get("/api/v1/feed", (req, res) => res.send("hello world"));
app.post("/api/v1/comment", (req, res) => {
  // req.body.comment
  res.send("hello world");
});
app.get("/hello-world", (req, res) => res.send("hello world"));

app.listen(port, () => console.log(`Listening on port ${port}`));
